/* eslint-disable no-restricted-syntax */
// ---Dependencys
import { ReactElement } from 'react';
import { Helmet } from 'react-helmet';
// ---Components
import { HealthCont } from 'Pages/Health/HealthCont/HealthCont';

/**
 * Health Component: Este componente es para dar datos al Helmet de
 * la pagina y concatenarlo con el contenedor de la pagina de "Health"
 * @returns { ReactElement } ReactElement
 */
export default function Health(): ReactElement {
  return (
    <>
      <Helmet>
        <title>Forge Template | Health</title>
      </Helmet>
      <HealthCont />
    </>
  );
}
