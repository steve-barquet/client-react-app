// ---Dependencies
import { KeyboardEvent, ReactElement, useState } from 'react';
// ---UI Dependencies
import { Button, Col, Input, Row } from 'antd';
// ---Redux
import { useHealthInfoStore } from '@Redux/healthInfo/store';

/**
 * Verifier Component: Do something
 * @returns {ReactElement}
 */
export function Verifier(): ReactElement {
  // -----------------------CONSTS, HOOKS, STATES
  const [pass, setPass] = useState('');
  const { patch } = useHealthInfoStore();

  // -----------------------MAIN METHODS
  /** Valida si el pass coincide */
  function validatePass() {
    if (process?.env?.REACT_APP_HEALTH_PASS === pass) {
      patch({ verified: true, lastPass: pass });
    }
  }
  /** Valida si el pass coincide (al presionar enter) */
  function onKeyPress(event: KeyboardEvent<unknown>) {
    if (event.key === 'Enter') {
      validatePass();
    }
  }
  // -----------------------RENDER
  if (process?.env?.REACT_APP_HEALTH_PASS)
    return (
      <div className="login">
        <Row gutter={[0, 10]}>
          <Col span={24}>
            <h3>Dev password:</h3>
          </Col>
          <Col span={24}>
            <Input.Password
              value={pass}
              onKeyDown={onKeyPress}
              onChange={(e) => setPass(e.target.value)}
            />
          </Col>
          <Col span={24}>
            <Button type="primary" size="large" onClick={validatePass}>
              Validar
            </Button>
          </Col>
        </Row>
      </div>
    );
  return <h3>&quot;REACT_APP_HEALTH_PASS&quot; env no seteada</h3>;
}
