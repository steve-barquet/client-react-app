// ---Dependency´s
import React from 'react';
// ---Config
import { isProd } from 'AppConfig/globalData';
// ---Components
import { FrontendInfo } from './FrontendInfo/FrontendInfo';
import { Verifier } from './Verifier/Verifier';
// ---Redux
import { useHealthInfoStore } from '@Redux/healthInfo/store';

/**
 * Componente HealthCont: Donde se mostrarán la pagina de error en caso de uno
 * @returns {ReactElement} ReactElement
 */
export function HealthCont(): React.ReactElement {
  const { lastPass, verified } = useHealthInfoStore(['lastPass', 'verified']);
  const showData =
    !isProd || (isProd && verified && process?.env?.REACT_APP_HEALTH_PASS === lastPass);

  return <div className="HealthCont">{showData ? <FrontendInfo /> : <Verifier />}</div>;
}
