// ---Dependency´s
import { ReactElement } from 'react';
// ---Routes
import { WrappedRoutes } from './Routes';
// ---Configs
import { GlobalProvider } from 'GlobalProvider/GlobalProvider';

/**
 * Raíz de la aplicación
 * @returns {ReactElement} ReactElement
 */
export function AppContainer(): ReactElement {
  return (
    <GlobalProvider>
      <WrappedRoutes />
    </GlobalProvider>
  );
}
