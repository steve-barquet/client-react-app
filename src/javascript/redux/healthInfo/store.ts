// ---Store Config
import { genericStore, storageBuilder } from '@Redux/config/genericStore';
// ---TYPES
import { HealthInfoInitial } from '@Redux/healthInfo/storeTypes';

const initialState: HealthInfoInitial = {
  verified: false,
  lastPass: 'holo',
};

/** Instancia genérica básica  */
export const useGenericZustand = genericStore({
  initialState,
  name: 'useHealthInfoStore',
  persist: true,
});

/** Hook que manipula el storage de "useHealthInfoStore" Nota: El nombre tiene que se único */
export const useHealthInfoStore = storageBuilder(initialState, useGenericZustand);
