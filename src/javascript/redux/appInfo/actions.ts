import { GenericStore } from '@Redux/config/genericStoreTypes';
import { AppInfoInitial } from './storeTypes';

/** Acciones adicionales para tu "useAppInfoStoreV3" */
export const customActions = (allStore: GenericStore<AppInfoInitial>) => {
  // --------------CUSTOM ACTIONS-----------------
  /** Update currentPath from store */
  function updatePath(currentPath: string) {
    allStore.patch({ currentPath });
  }
  /** Update currentPath from store */
  function updateBotz(currentPath: string) {
    allStore.patch({ currentPath });
  }
  return {
    updatePath,
    updateBotz,
  };
};

/* Nota: No se están utilizando, solo se crearon como ejemplo */
