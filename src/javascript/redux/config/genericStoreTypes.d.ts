export type GenericStore<InitialState> = InitialState & {
  /** Recibe un objeto con props opcionales del estado de tu storage y actualiza sólo las props enviadas */
  patch: (data: Partial<InitialState>) => void;
  /** Recibe un objeto con todas las props del estado de tu storage y actualiza el estado de tu storage completo */
  update: (data: InitialState) => void;
  /** Reinicia el estado de tu storage a su estado inicial */
  clear: () => void;
};

export type GenericPatch<T> = (data: Partial<T>) => void;

export interface StorageConfig<T> {
  /** Nombre único para el storage en memoria */
  name: string;
  /** Callback que retorna un objeto con las custom actions (opcional) */
  initialState: T;
  /** Persiste el storage en localstorage, @default false, Nota importante: NextJS pre-genera el sitio antes de llegar al local storage por lo que los metodos de cambios de estado se vuelven asíncronos y se ejecutan hasta llegar al cliente, es importante tener bien inicializado el estado para que no se rompa algo de la app en lo que se llega al cliente */
  persist?: boolean;
}

export type CustomActions<InitialState, Actions> = (
  allStore: GenericStore<InitialState>,
) => Actions;

export type Actions<T, K> = K & {
  patch: (data: Partial<T>) => void;
  clear: () => void;
  update: (data: T) => void;
};

export type CustomHookReturn<T, K> = GenericStore<T> & Actions<T, K>

/** Callback que retorna un objeto con las custom actions (opcional) */
export type CustomHook<T, K> = (stateProps?: (keyof T)[]) => CustomHookReturn<T, K>;



export type CustomHookPersisted<T, K> = (stateProps?: (keyof T)[]) =>Promise<CustomHookReturn>;
