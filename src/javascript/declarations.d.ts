export declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  };
  
  module '*.png';
  module '*.webp';
  module '*.jpg';
}

/** Any object */
export type RandObj = { [s: string]: unknown; }

/** Any array */
export type RandArray = any[] | Array<unknown>

/** Recibe una interfaz y convierte todas sus props a props opcionales */
export type Partial<T> = {
  [P in keyof T]?: T[P];
};

/** Extrae los types de los parámetros de una función */
export type ArgumentTypes<F extends Function> = F extends (...args: infer A) => any ? A : never;