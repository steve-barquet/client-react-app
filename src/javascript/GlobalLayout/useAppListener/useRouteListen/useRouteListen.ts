// ---Dependencies
import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
// ---Redux
import { useAppInfoStoreV3 } from '@Redux/appInfo/store';

/** Hook para escuchar y capturar la ruta actual y los parámetros en la url de la aplicación */
export function useRouteListen() {
  const { currentParams, patch } = useAppInfoStoreV3(['currentParams']);
  const { pathname: currentPath, search: urlParams } = useLocation();
  useEffect(() => {
    patch({ currentPath });
  }, [currentPath]);

  useEffect(() => {
    const justParams = urlParams.split('?')[1];
    if (currentParams !== justParams) {
      patch({ currentParams: justParams });
    }
  }, [urlParams]);
}
