// ---Dependencies
import { Button, Col, Row } from 'antd';
// ---UI Dependencies
import { ReactElement } from 'react';
import { Link } from 'react-router-dom';
// ---Utils
import { responsiveBasicGrid } from 'Utils/functions/responsiveUtils';

const gridProps = responsiveBasicGrid(8);
/**
 * Navbar Component: Ejemplo de navbar para la aplicación
 * @returns {ReactElement} ReactElement
 */
export function Navbar(): ReactElement {
  // -----------------------RENDER
  return (
    <nav className="Navbar">
      <Row>
        <Col {...gridProps}>
          <h3>Navbar</h3>
        </Col>
        <Col {...gridProps}>
          <Link to="/">
            <Button type="link">Home</Button>
          </Link>
        </Col>
        <Col {...gridProps}>
          <Link to="/health">
            <Button type="link">Health</Button>
          </Link>
        </Col>
      </Row>
    </nav>
  );
}
