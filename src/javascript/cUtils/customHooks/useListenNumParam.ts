// ---Redux
import { useAppInfoStoreV3 } from '@Redux/appInfo/store';

/**
 * Obtenga una identificación o cualquier número de la URL y lo devuelve como un número.
 * Example: http://example.com/route?23 retrives 23 as number
 * @returns {numbrer}
 */
export function useListenNumParam(): number {
  // -----------------------CONSTS, HOOKS, STATES
  const { currentParams } = useAppInfoStoreV3(['currentParams']);
  const id = Number(currentParams?.substring(1, currentParams?.length));
  return id;
}
