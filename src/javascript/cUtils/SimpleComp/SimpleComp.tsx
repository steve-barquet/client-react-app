// ---Dependencies
import { ReactElement } from 'react';
// ---UI Dependencies
// ---Custom Hooks
// ---Redux
// ---Components
// ---AppConfig
// ---Assets
// ---Utils
// ---Requests
// ---Images

interface Props {}
/**
 * SimpleComp Component: Do something
 * @returns {ReactElement}
 */
export function SimpleComp(props: Props): ReactElement {
  // -----------------------CONSTS, HOOKS, STATES
  // -----------------------MAIN METHODS
  // -----------------------AUX METHODS
  // -----------------------RENDER
  return <p>SimpleComp</p>;
}
