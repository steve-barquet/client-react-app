import { ReactNode } from 'react';
import { ConfigProvider, theme } from 'antd';
// ---Config
import { appColors } from 'AppConfig/globalData';

interface Props {
  children: ReactNode;
}
/**
 * AntdProvider component: Proveedor de tema de antd, parra editar gama de colores
 * @returns
 */
export function AntdProvider({ children }: Props) {
  const lessColors = appColors;
  return (
    <ConfigProvider
      theme={{
        algorithm: theme.defaultAlgorithm,
        token: {
          colorPrimary: lessColors.primary,
        },
      }}
    >
      {children}
    </ConfigProvider>
  );
}
