// ---Dependencies
import { ReactElement, ReactNode } from 'react';
// ---Components
import { BrowserRouter } from 'react-router-dom';
import { AntdProvider } from './AntdProvider/AntdProvider';

interface Props {
  children: ReactNode;
}
/**
 * GlobalProvider Component: Componente que encapsula todos los providers de la app
 * @returns {ReactElement}
 */
export function GlobalProvider({ children: app }: Props): ReactElement {
  // -----------------------RENDER
  return (
    <BrowserRouter>
      <AntdProvider>{app}</AntdProvider>
    </BrowserRouter>
  );
}
