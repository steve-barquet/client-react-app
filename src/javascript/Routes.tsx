/* eslint-disable react/jsx-no-useless-fragment */
/* eslint-disable react/jsx-fragments */
// ---Dependencys
import { ReactElement, Fragment, lazy, Suspense } from 'react';
import { Route, Routes } from 'react-router-dom';
// ---Components
import { GlobalLayout } from 'GlobalLayout/GlobalLayout';
// ---Pages
const HomePage = lazy(() => import('Pages/Home/HomePage'));
const Error404Page = lazy(() => import('Pages/Error404/Error404Page'));
const Health = lazy(() => import('Pages/Health/Health'));

/**
 * WrappedRoutes Component: Representar componentes como rutas de la aplicación, también es la raíz de toda la aplicación,
 * Obtenga datos útiles como el tamaño de la ventana de la aplicación, la ruta actual y recupere los datos para redux
 * @returns {ReactElement} ReactElement
 */
export function WrappedRoutes(): ReactElement {
  return (
    <Fragment>
      <GlobalLayout>
        <Suspense fallback={<h3>Loading...</h3>}>
          <Routes>
            <Route path="/" element={<HomePage />} />
            <Route path="/health" element={<Health />} />
            <Route path="*" element={<Error404Page />} />
          </Routes>
        </Suspense>
      </GlobalLayout>
    </Fragment>
  );
}
