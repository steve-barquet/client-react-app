/* Constantes que contienen configuraciones globales del proyecto */
export const ownerData = {
  domain: 'example.com',
  title: {
    main: 'Shelly',
    sub: 'store',
    shortAppName: 'Shelly',
  },
  phoneStr: '5555555555',
  phoneMain: 5555555555,
};
