module.exports = (plop) => {
  plop.setGenerator('component', {
    description: 'Create a component',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message:
          'Cual es el nombre de tus componentes (sin Prefijos o Sufijos)? ... (recuerda agregar la page en routes.tsx)',
      },
    ],
    actions: [
      {
        type: 'add',
        path: '../src/javascript/cPages/{{pascalCase name}}/{{pascalCase name}}.tsx',
        templateFile: 'templates/page.tsx.hbs',
      },
      {
        type: 'add',
        path: '../src/javascript/cPages/{{pascalCase name}}/{{pascalCase name}}Cont/{{pascalCase name}}Cont.tsx',
        templateFile: 'templates/Container.tsx.hbs',
      },
    ],
  });
};
