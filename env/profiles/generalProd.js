const { getLessVars } = require('../configs/getLessVars');
/* const { untrackedEnvs } = require('../untracked/env'); */

/**
 * - En la linea 5 está el ejemplo de como leer envs desde la terminal
 * - Todos los nombres deben llevar el prefijo "REACT_APP_" para que la app pueda leer las envs
 */
exports.generalProd = {
  /** Password para acceder al health endpoint */
  REACT_APP_HEALTH_PASS:
    process.env?.REACT_APP_HEALTH_PASS /* || untrackedEnvs?.REACT_APP_HEALTH_PASS */,
  /** Nombre del perfil de 'env' */
  REACT_APP_PROFILE: 'generalProd',
  /** Puerto donde corre el server de desarrollo */
  REACT_APP_PORT: 8080,
  /** URL del backend */
  REACT_APP_API_URL: 'http://localhost:4000',
  /** Nombre de la app */
  REACT_APP_APP_NAME: 'REACT APP PRODUCTION',
  /** Carga automáticamente las variables 'colors' de less como env */
  REACT_APP_LESS_COLORS: getLessVars(),
};
