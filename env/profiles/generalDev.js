const { getLessVars } = require('../configs/getLessVars');

/**
 * - En la linea 5 está el ejemplo de como leer envs desde la terminal
 * - Todos los nombres deben llevar el prefijo "REACT_APP_" para que la app pueda leer las envs
 */
exports.generalDev = {
  /** Nombre del perfil de 'env' */
  REACT_APP_PROFILE: 'generalDev',
  /** Puerto donde corre el server de desarrollo */
  REACT_APP_PORT: 3000,
  /** URL del backend */
  REACT_APP_API_URL: 'http://localhost:4000',
  /** Nombre de la app */
  REACT_APP_APP_NAME: 'REACT APP DEVELOPMENT',
  /** Carga automáticamente las variables 'colors' de less como env */
  REACT_APP_LESS_COLORS: getLessVars(),
};
